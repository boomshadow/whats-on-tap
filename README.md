# Whats on tap
Dead simple web server to show what beers are on tap.

There are a few projects/products already out there (See the section: [Other Projects/Products](#other-projects-products)), but they are either not maintained anymore, cloud-hosted, or pay-for.

"Whats on tap" was created for those that want a free, self-hosted beer dashboard, but don't want all the bells & whistles (complexity). If you simply want a web interface to update and show beers on tap, and nothing more, this is for you.

This project is _probably not_ for you if you are looking for any more advanced features such as: keg monitoring, flow control, user logins, drinker accounts, leaderboards, social media integration, etc...

If you are looking for those features, either check out [Other Projects/Products](#other-projects-products), or submit a merge request to add the feature you want (See: [Contributing](#contributing).

# Features/Goals
Basic features needed for this project:

- [ ] Administration without a login
- [ ] Create new beer entries (local, user-creatable database of beers)
- [ ] Select which beers are currently on tap
- [ ] Ship easily (Docker)
- [ ] Mobile friendly (responsive design)

Possible future goals:
- [ ] Able to import from external open database for pictures, descriptions, ABV, IBU, etc...

# Contributing
You can contribute by logging 'issues' or help with coding. If you are doing code contributions, please make sure you create an 'issue' for the feature you're working on, and submit a merge request.

The idea is to keep this application simple, so if you add a feature, try to keep simplicity in mind. Ideally, you should include a mechanism for disabling the feature for those that might not want it.

# Other Projects/Products
- [Kegbot](https://github.com/Kegbot) -- unmaintained
- [RaspberryPints](https://github.com/RaspberryPints/RaspberryPints) -- unmaintained 
- [Taplist.io](https://taplist.io) -- no self-hosting

# Credit
Icon: [Beer Tap by Luka Purgar](https://thenounproject.com/term/beer-tap/582415/) from the Noun Project
